Recipe categories:
-------------------

#### Meal Type:
- Breakfast/Brunch
- Lunch 
- Dinner
- Dessert
- Snack

#### Dish Type:
- Quick
- Baking
- Bread
- Curries
- Stews/Chili
- Smoothies

#### Cooking Style:
- BBQ
- Instant Pot/Pressure Cooker
- Slow Cooker
- Oven
- Stove-top
- No-bake

#### Health:
- Vegetarian
- Gluten free
- Paleo
- Kid friendly

#### Ingredient:
- Chicken
- Beef
- Pasta

#### User Collections
