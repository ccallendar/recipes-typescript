const defaultOptions = {
    name: "default",
    cli: {
        entitiesDir: "./src/imports/models",
        migrationsDir: "./src/dbmigrations",
        subscribersDir: "./src/subscribers",
    },
    entities: ["./dist/imports/models/*.js"],
    subscribers: ["./dist/subscribers/*.js"],
    database: 'recipes',
    host: 'localhost',
    port: 5432,
    username: 'recipemaster',
    password: 'recipemaster',
    logging: ["error"],
    dropSchema: false,
    migrations: ["./dist/dbmigrations/*.js"],
    migrationsTableName: "recipes_db_migration",
    synchronize: false,
    type: "postgres",
};

// reset database state when running tests
// create schema on-the-fly
const testOptions = Object.assign(
    { ...defaultOptions },
    {
        name: "test",
        database: `recipes_test`,
        dropSchema: true,
        synchronize: true,
    }
);

module.exports = [defaultOptions, testOptions];
