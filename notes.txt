Recipes:
------------
_id
title
dateAdded
dateUpdated
url
relatedUrl
photoUrl
rating? Or just on a meal?
notes


Meals:
--------
_id
date
dateAdded
recipeId
rating
notes
breakfast/lunch/dinner?
list of items to buy?



PAGES:
--------

1. Calendar page
Display meals - past and future
Clicking on a meal - takes you to the meal/recipe page

2. Meal/Recipe Page - View

3. Meal/Recipe page - Edit

4. Recipe List page
Clicking on a recipe takes you to the Meal/Recipe View page
Has an Add New button - takes you to Meal/Recipe Edit page



Dashboard:
-----------
Upcoming meals
Most often made meals
Highest rating
