import Database from '../src/server/db/database';
import {Recipe} from "../src/imports/model/recipe";

// export default class Testdb {
//     static dbTest() {
//         const db = new Database();
//         db.getAllRecipes();
//         return "asdf";
//     }
// }

console.log("==================");
console.log("Server test:");
// console.log("DB Test", Testdb.dbTest());
const db = new Database();
db.getAllRecipes().then((recipes: Array<Recipe>) => {
    const recipe: Recipe = {
        id: 0,
        name: "Insert test - " + new Date().getTime(),
        browserName: "insert-test",
        description: "",
        urls: ["http://google.ca", "http://amazon.com"],
    };
    return db.upsertRecipe(recipe);
}).then(() => {
    console.log("Done.");
    console.log("==================");
}).catch((ex) => {
    console.log("error", ex);
    console.log("==================");
});

