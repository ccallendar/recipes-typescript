DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS comments;
DROP TABLE IF EXISTS directions;
DROP TABLE IF EXISTS ingredients;
DROP TABLE IF EXISTS photos;
DROP TABLE IF EXISTS ratings;
DROP TABLE IF EXISTS urls;
DROP TABLE IF EXISTS recipes;

CREATE TABLE recipes
(
    id SERIAL NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    browser_name character varying(64) COLLATE pg_catalog."default" NOT NULL,
    date_created timestamp without time zone NOT NULL,
    date_updated timestamp without time zone NOT NULL,
    description text COLLATE pg_catalog."default",
    prep_time integer,
    cook_time integer,
    favorite boolean,
    CONSTRAINT recipes_pkey PRIMARY KEY (id),
    CONSTRAINT browser_name_unique UNIQUE (browser_name)
);

CREATE TABLE categories
(
    id SERIAL NOT NULL,
    recipe_id integer NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT categories_pkey PRIMARY KEY (id),
    CONSTRAINT categories_recipe_id_fkey FOREIGN KEY (recipe_id)
            REFERENCES recipes (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);
CREATE INDEX ON categories (name);

CREATE TABLE comments
(
    id SERIAL NOT NULL,
    recipe_id integer NOT NULL,
    comment text COLLATE pg_catalog."default" NOT NULL,
    date timestamp without time zone NOT NULL,
    CONSTRAINT comments_pkey PRIMARY KEY (id),
    CONSTRAINT comments_recipe_id_fkey FOREIGN KEY (recipe_id)
            REFERENCES recipes (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE directions
(
    id SERIAL NOT NULL,
    recipe_id integer NOT NULL,
    direction text COLLATE pg_catalog."default" NOT NULL,
    "order" smallint NOT NULL,
    CONSTRAINT directions_pkey PRIMARY KEY (id),
    CONSTRAINT directions_recipe_id_fkey FOREIGN KEY (recipe_id)
            REFERENCES recipes (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE ingredients
(
    id SERIAL NOT NULL,
    recipe_id integer NOT NULL,
    amount real,
    units text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT ingredients_pkey PRIMARY KEY (id),
    CONSTRAINT ingredients_recipe_id_fkey FOREIGN KEY (recipe_id)
            REFERENCES recipes (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TABLE photos
(
    id SERIAL NOT NULL,
    recipe_id integer NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    alt character varying(64) COLLATE pg_catalog."default",
    width smallint,
    height smallint,
    is_primary boolean NOT NULL,
    CONSTRAINT photos_pkey PRIMARY KEY (id),
    CONSTRAINT photos_recipe_id_fkey FOREIGN KEY (recipe_id)
        REFERENCES recipes (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE ratings
(
    id SERIAL NOT NULL,
    recipe_id integer NOT NULL,
	meal_id integer,
    rating smallint NOT NULL,
    comment text COLLATE pg_catalog."default",
    date timestamp without time zone NOT NULL,
    CONSTRAINT ratings_pkey PRIMARY KEY (id),
    CONSTRAINT ratings_recipe_id_fkey FOREIGN KEY (recipe_id)
        REFERENCES recipes (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
	CONSTRAINT ratings_meal_id_fkey FOREIGN KEY (meal_id)
        REFERENCES meals (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE urls
(
    id SERIAL NOT NULL,
    recipe_id integer NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT urls_pkey PRIMARY KEY (id),
    CONSTRAINT urls_recipe_id_fkey FOREIGN KEY (recipe_id)
            REFERENCES recipes (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);

CREATE TYPE meal_type AS ENUM ('breakfast', 'brunch', 'lunch', 'dinner', 'dessert', 'snack');

CREATE TABLE meals
(
    id SERIAL NOT NULL,
    recipe_id integer,
    date date NOT NULL,
    date_added timestamp without time zone NOT NULL,
    type meal_type,
    notes text COLLATE pg_catalog."default",
    CONSTRAINT meals_pkey PRIMARY KEY (id),
    CONSTRAINT meals_recipe_id_fkey FOREIGN KEY (recipe_id)
            REFERENCES recipes (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION
);
