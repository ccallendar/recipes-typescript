import fs from "fs-extra";
import {Client} from "pg";

const init = async () => {
    // read environment variables
    const client = new Client({
        host: 'localhost',
        database: 'recipes',
        user: 'recipemaster',
        password: 'recipemaster',
        port: 5432,
    });
    try {
        await client.connect();
        const sql = await fs.readFile("./tools/initdb.pgsql", {encoding: "UTF-8"});
        const statements = sql.split(/;\s*$/m);
        for (const statement of statements) {
            if (statement.length > 3) {
                await client.query(statement);
            }
        }
    } catch (err) {
        console.log(err);
        throw err;
    } finally {
        await client.end();
    }
};

init().then(() => {
    console.log("Finished DB init script");
}).catch((ex) => {
    console.log("Finished DB init script with errors", ex);
});