import dispatcher from '../dispatcher';

export function reloadCategories() {
    dispatcher.dispatch({
        type: "RELOAD_CATEGORIES",
    });
}
