import dispatcher from '../dispatcher';
import {Recipe} from '../../imports/model/recipe';

export function createRecipe(recipe: Recipe) {
    dispatcher.dispatch({
        type: "CREATE_RECIPE",
        recipe,
    });
}

export function updateRecipe(recipe: Recipe) {
    dispatcher.dispatch({
        type: "UPDATE_RECIPE",
        recipe,
    });
}

export function reloadRecipes() {
    dispatcher.dispatch({
        type: "RELOAD_RECIPES",
    });
}
