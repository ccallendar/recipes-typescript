import React from "react";
import {useParams} from "react-router-dom";
import NewForm from "../components/NewForm";

interface EditRecipePageProps {

}

const EditRecipePage: React.FC<EditRecipePageProps> = (/* props: EditRecipePageProps */) => {
    let {id} = useParams();
    console.log("** EditRecipePage", id);
    id = parseInt(id);
    if (!(id > 0)) {
        id = 0;
    }
    console.log(id);
    return (
        <NewForm recipeId={id} />
    );
}

export default EditRecipePage;