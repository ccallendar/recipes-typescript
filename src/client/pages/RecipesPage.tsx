import React from "react";
import RecipesList from "../components/RecipesList";

interface RecipesPageProps {

}

export default class RecipesPage extends React.Component<RecipesPageProps> {
    render() {
        return (
            <RecipesList />
        );
    }
}