import React from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import {reloadCategories} from "./actions/category-actions";
import {reloadRecipes} from "./actions/recipe-actions";
import Nav from "./components/Nav";
import Dashboard from "./pages/Dashboard";
import RecipesPage from "./pages/RecipesPage";
import EditRecipePage from "./pages/EditRecipePage";
import './App.css';

class App extends React.PureComponent {
    componentDidMount() {
        reloadRecipes();
        reloadCategories();
    }
    render() {
        return (
            <BrowserRouter>
              <div className="recipes-app">
                <header className="recipes-header">
                  <Nav />
                </header>
                <main className="recipes-content">
                    <Switch>
                        <Route exact path="/">
                            <Dashboard />
                        </Route>
                        <Route exact path="/recipes">
                            <RecipesPage />
                        </Route>
                        <Route exact path="/recipes/:id">
                            <EditRecipePage />
                        </Route>
                    </Switch>

                </main>
              </div>
            </BrowserRouter>
        );
    }
}

export default App;
