import React from "react";
// @ts-ignore
import CreatableSelect from 'react-select/creatable';
import categoryStore from "../stores/category-store";
import categories from "../../data/categories.json";
import {ValueType} from "react-select";

interface CategoryMultiSelectProps {
    onChange?: Function;
    className?: string;
    initialValue: Array<string>;
}

interface CategoryMultiSelectState {
    value: Array<Option>;
    options: Array<Option>;
    isLoading: boolean;
}

type Option = {
    value: string;
    label: string;
    isDisabled?: boolean;
}

const SEPARATOR_CATEGORY: Option = {
    value: "",
    label: "-",
    isDisabled: true,
};

function getOptions(names: Array<string>): Array<Option> {
    const seen = new Set<string>();
    const options = categories.map(name => {
        if (name === "-") {
            return SEPARATOR_CATEGORY;
        }
        seen.add(name);
        return {
            value: name,
            label: name,
        };
    });
    names.forEach((name) => {
        if (name && !seen.has(name)) {
            options.push({
                value: name,
                label: name,
            });
        }
    });
    return options;
}

export default class CategoryMultiSelect extends React.Component<CategoryMultiSelectProps, CategoryMultiSelectState> {
    constructor(props: CategoryMultiSelectProps) {
        super(props);
        const {initialValue} = props;
        let value = (initialValue || []).map(name => {
            return {
                value: name,
                label: name,
            };
        });
        this.state = {
            value,
            options: [],
            isLoading: true,
        };
    }
    componentDidMount() {
        categoryStore.on("loading", this.setLoading);
        categoryStore.on("change", this.getCategories);
        this.getCategories();
    }
    componentWillUnmount() {
        categoryStore.removeListener("loading", this.setLoading);
        categoryStore.removeListener("change", this.getCategories);
    }
    getCategories = () => {
        const options: Array<Option> = getOptions(categoryStore.getAll());
        this.setState({options, isLoading: false});
    };
    setLoading = () => {
        this.setState({isLoading: true});
    };
    changeHandler = (newValue: ValueType<Option>/*, actionMeta: any*/) => {
        let value: Array<Option> = []
        if (newValue && (newValue instanceof Array)) {
            value = (newValue as Array<Option>);
        }
        this.setState({value}, () => {
           this.valueChanged();
        });
    };
    createHandler = (newValue: string) => {
        let {options, value} = this.state;
        newValue = newValue.trim();
        if (newValue && (newValue !== "-")) {
            const newOption = { value: newValue, label: newValue };
            const lowerValue = newValue.toLowerCase();
            let exists = options.find((opt) => {
                return (opt.value.toLowerCase() === lowerValue);
            });
            if (!exists) {
                options = options.concat();
                options.push(newOption);
                this.setState({options});
            }
            exists = value.find((opt) => {
                return (opt.value.toLowerCase() === lowerValue);
            });
            if (!exists) {
                value = value.concat();
                value.push(newOption);
                this.setState({value}, () => {
                    this.valueChanged();
                });
            }
        }
    };
    valueChanged() {
        if (this.props.onChange) {
            const {value} = this.state;
            const names: Array<string> = value.map(sel => sel.value);
            this.props.onChange.call(this, names);
        }
    }
    render() {
        const {className} = this.props;
        const {isLoading, options, value} = this.state;
        return (
            <CreatableSelect
                className={className}
                classNamePrefix={className}
                isMulti
                isClearable
                isLoading={isLoading}
                options={options}
                value={value}
                readonly
                onChange={this.changeHandler}
                onCreateOption={this.createHandler}
            />
        )
    }
}