import React from "react";
import {Ingredient} from "../../imports/model/ingredient";

type IngredientItemProps = {
    ingredient: Ingredient;
}

const IngredientItem: React.FC<IngredientItemProps> = (props: IngredientItemProps) => {
    const {ingredient} = props;
    let str = "";
    if (ingredient.amount != null) {
        str = `${str}${ingredient.amount}`;
    }
    if (ingredient.units) {
        str = `${str}${ingredient.units}`;
    }
    str = `${str}${ingredient.name}`;
    return (
        <li className="ingredient-item">{str}</li>
    );
}
export default IngredientItem;