import React from "react";
import {Direction} from "../../imports/model/direction";

type DirectionItemProps = {
    direction: Direction;
}

const DirectionItem: React.FC<DirectionItemProps> = (props: DirectionItemProps) => {
    const {direction} = props;
    return (
        <li className="ingredient-item">{direction.direction}</li>
    );
};

export default DirectionItem;