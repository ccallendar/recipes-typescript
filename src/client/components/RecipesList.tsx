import React from 'react'
import {Recipe} from '../../imports/model/recipe';
import recipeStore from '../stores/recipe-store';
import './RecipesList.css';
import RecipeItem from "./RecipeItem";

interface RecipesListProps {

}

interface RecipeListState {
    recipes: Recipe[];
    loading: boolean;
}

export default class RecipesList extends React.Component<RecipesListProps, RecipeListState> {
    constructor(props: RecipesListProps) {
        super(props);
        this.state = {
            recipes: [],
            loading: false,
        };
    }
    componentDidMount() {
        recipeStore.on("loading", this.setLoading);
        recipeStore.on("change", this.getRecipes);
        this.getRecipes();

    }
    componentWillUnmount() {
        recipeStore.removeListener("loading", this.setLoading);
        recipeStore.removeListener("change", this.getRecipes);
    }
    setLoading = () => {
        this.setState({loading: true});
    };
    getRecipes = () => {
        this.setState({recipes: recipeStore.getAll(), loading: false});
    };
    render() {
        const {loading, recipes} = this.state;
        let recipeItems: JSX.Element[];
        if (loading) {
            recipeItems = [<li key="loading">Loading...</li>];
        } else {
            recipeItems = recipes.map((recipe: Recipe) => {
                const key = `recipe-${recipe.id}`;
                return <RecipeItem key={key} recipe={recipe} condensed />;
            });
            if (recipeItems.length === 0) {
                recipeItems = [<li key="-">No recipes</li>];
            }
        }
        return (
            <div className="recipe-list-container">
                <h2>Recipes</h2>
                <ul className="recipe-list">{recipeItems}</ul>
            </div>
        );
    }
}