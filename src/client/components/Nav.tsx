import React from 'react';
import {NavLink} from "react-router-dom";
import './nav.scss';

export default class Nav extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark recipes-nav">
                <a className="navbar-brand" href="/">
                    <img src="/recipes.svg" className="recipe-icon" alt="Recipe"/>
                    Recipes
                </a>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarCollapseContainer"
                    aria-controls="navbarCollapseContainer"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse" id="navbarCollapseContainer">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink exact to="/" className="nav-link" activeClassName="active">Home</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink exact to="/recipes" className="nav-link" activeClassName="active">Recipes</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink exact to="/recipes/new" className="nav-link" activeClassName="active">Add New Recipe</NavLink>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}