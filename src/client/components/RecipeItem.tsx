import React from "react";
import {Recipe} from "../../imports/model/recipe";
import {NavLink} from "react-router-dom";
import IngredientsList from "./IngredientsList";
import DirectionsList from "./DirectionsList";

const MAX_WIDTH = 200;
const MAX_HEIGHT = 200;
function calculateImageStyle(width: number|undefined, height: number|undefined): Object {
    const style = {
        width: '0',
        height: '0',
    };
    if ((width !== undefined) && (width > 0) && (height !== undefined) && (height > 0)) {
        let w = width;
        let h = height;
        if ((width > MAX_WIDTH) || (height > MAX_HEIGHT)) {
            const wRatio = width / MAX_WIDTH;
            const hRatio = height / MAX_HEIGHT;
            if (wRatio >= hRatio) {
                w = MAX_WIDTH;
                h = height / wRatio;
            } else {
                w = width / hRatio;
                h = MAX_HEIGHT;
            }
        }
        style.width = `${w}px`;
        style.height = `${h}px`;
    }
    return style;
}

type RecipeItemProps = {
    recipe: Recipe;
    condensed?: boolean;
}

const RecipeItem: React.FC<RecipeItemProps> = (props: RecipeItemProps) => {
    const {recipe, condensed = false} = props;
    let date: string = "";
    if (recipe.dateUpdated) {
        date = new Date(recipe.dateUpdated).toLocaleDateString();
    }
    let imgSrc = "/meal.svg";
    let imgClass = "recipe-icon";
    let alt = "Recipe";
    let imgStyle = {};
    if (recipe.photos && recipe.photos.length) {
        const [photo] = recipe.photos;
        imgSrc = photo.url;
        imgClass = "recipe-photo";
        imgStyle = calculateImageStyle(photo.width, photo.height);
        if (photo.alt) {
            ({alt} = photo);
        }
    }
    let {description = ""} = recipe;
    const html = {__html: description.replace(/\n/g, "<br />")};

    const showDetails = !condensed;
    return (
        <li className="recipe-list-item">
            <NavLink exact to={`/recipes/${recipe.id}`} className="recipe-item" >
                <div className="recipe-image-holder">
                    <img src={imgSrc} className={`recipe-image ${imgClass}`} alt={alt} style={imgStyle} />
                </div>
                <div className="recipe-item-text">
                    <div className="recipe-name" role="button">{recipe.name}</div>
                    <div className="recipe-date">{date}</div>
                    <div className="recipe-desc" dangerouslySetInnerHTML={html} />
                </div>
                {showDetails && <IngredientsList ingredients={recipe.ingredients} />}
                {showDetails && <DirectionsList directions={recipe.directions} />}
            </NavLink>
        </li>
    );
}
export default RecipeItem;