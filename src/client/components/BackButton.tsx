import {Route, withRouter, RouteComponentProps} from "react-router-dom";
import React from "react";

interface BackButtonProps extends RouteComponentProps<any> {
    className?: string;
}

const BackButton: React.FC<BackButtonProps> = (props: BackButtonProps) => {
    const {className = "btn"} = props;
    return (
        <Route render={({history}) => (
            <button className={className} type='button' onClick={() => {
                history.goBack()
            }}>Back</button>
        )}/>
    );
};

export default withRouter(BackButton);