import React from "react";
import {Ingredient} from "../../imports/model/ingredient";
import IngredientItem from "./IngredientItem";

type IngredientsListProps = {
    ingredients?: Array<Ingredient>;
    condensed?: boolean;
}

export default class IngredientsList extends React.PureComponent<IngredientsListProps> {
    render() {
        const {ingredients, condensed} = this.props;
        if (!ingredients || !ingredients.length) {
            return null;
        }
        return (
            <div className="recipe-ingredients">
                <h5>Ingredients</h5>
                <ul className={`ingredients-list ${condensed ? 'condensed' : ''}`}>
                    {ingredients.map(ingredient => <IngredientItem ingredient={ingredient} key={ingredient.id} />)}
                </ul>
            </div>
        )
    }
}