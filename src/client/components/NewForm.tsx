import React from 'react'
import { withRouter, RouteComponentProps } from 'react-router-dom';
import {Photo} from "../../imports/model/photo";
import {Recipe} from "../../imports/model/recipe";
import {joinLines} from "../../imports/utils/StringUtils";
import {convertUrls, convertIngredients, convertDirections} from "../../imports/utils/ModelUtils";
import {createRecipe, updateRecipe} from '../actions/recipe-actions';
import recipeStore from "../stores/recipe-store";
import CheckBox from "./CheckBox";
import './new-form.scss';
import CategoryMultiSelect from "./CategoryMultiSelect";
import BackButton from "./BackButton";

function loadImages(str: string, removeQuery: boolean = true):Promise<Array<Photo>> {
    const urls = convertUrls(str, removeQuery);
    const promises: Array<Promise<Photo>> = urls.map((line: string, idx: number) => {
        return loadImage(line, idx === 0);
    });
    console.log(`Loading ${promises.length} images...`);
    return Promise.all(promises);
}

function loadImage(url: string, isPrimary = false, alt = ''):Promise<Photo> {
    return new Promise(function(resolve, reject) {
        if (url && url.startsWith("http")) {
            const photo: Photo = {url, alt, isPrimary, width: -1, height: -1};
            const img = new Image();
            img.onload = () => {
                photo.width = img.width;
                photo.height = img.height;
                console.log("Loaded photo", url, img.width, img.naturalWidth, img.height, img.naturalHeight);
                resolve(photo);
            };
            img.onerror = function(ex) {
                console.log("Failed to load photo", ex);
                reject(ex);
            };
            img.src = url;
        } else {
            reject(new Error("Invalid photo url"));
        }
    });
}


interface NewFormProps extends RouteComponentProps<any> {
    recipeId: number;
}

interface EditRecipe {
    id: number;
    name: string;
    description: string;
    urls: string;
    photos: string;
    ingredients: string;
    directions: string;
    categories: Array<string>;
}

interface NewFormState {
    recipe: EditRecipe;
    cleanUrls: boolean;
    cleanPhotoUrls: boolean;
    editMode: boolean;
}

class NewForm extends React.Component<NewFormProps, NewFormState> {
    constructor(props: NewFormProps) {
        super(props);
        const {recipeId} = this.props;
        const recipe: EditRecipe = NewForm.getEditRecipe(recipeId);
        this.state = {
            recipe,
            cleanUrls: true,
            cleanPhotoUrls: true,
            editMode: false,
        };
    }

    static getDerivedStateFromProps(nextProps: NewFormProps, prevState: NewFormState) {
        const {recipeId} = nextProps;
        const {recipe} = prevState;
        if (recipeId !== recipe.id) {
            return NewForm.getEditRecipe(recipeId);
        }
        return null;
    }
    componentDidMount() {
        recipeStore.on("change", this.getRecipe);
        recipeStore.on("added", this.newRecipeAdded);
        this.getRecipe();
    }
    componentWillUnmount() {
        recipeStore.removeListener("change", this.getRecipe);
        recipeStore.removeListener("added", this.newRecipeAdded);
    }
    static getEditRecipe(id: number): EditRecipe {
        const recipe: Recipe|undefined = recipeStore.get(id);
        if (recipe) {
            return {
                id,
                name: recipe.name || '',
                description: recipe.description || '',
                urls: joinLines(recipe.urls),
                photos: joinLines(recipe.photos, "url"),
                ingredients: joinLines(recipe.ingredients, "name"),
                directions: joinLines(recipe.directions, "direction"),
                categories: recipe.categories || [],
            };
        }
        return {
            id,
            name: '',
            description: '',
            urls: '',
            photos: '',
            ingredients: '',
            directions: '',
            categories: [],
        };
    }
    getRecipe = () => {
        let {id} = this.state.recipe;
        console.log(`getRecipe(${id})`);
        const recipe: EditRecipe = NewForm.getEditRecipe(id);
        this.setState({recipe});
    };
    newRecipeAdded = (recipe: Recipe): void => {
        this.props.history.push(`/recipes/${recipe.id}`);
    };
    cancelEditHandler = () => {
        // TODO warn if changes?
        // Replace with store version
        this.getRecipe();
        this.setState({editMode: false});
    };
    editHandler = () => {
        this.setState({editMode: true});
    };
    saveHandler = (ev: React.MouseEvent): void => {
        const {recipe} = this.state;
        const {cleanUrls, cleanPhotoUrls} = this.state;
        if (recipe.name) {
            console.log(`** Saving recipe ${recipe.id}, '${recipe.name}', loading photos first`);
            loadImages(recipe.photos, cleanPhotoUrls).then((photos: Array<Photo>) => {
                console.log(`Loaded ${photos.length} photos`, photos);
                const recipeObj: Recipe = {
                    id: recipe.id,
                    name: recipe.name.trim(),
                    description: recipe.description.trim(),
                    photos,
                    urls: convertUrls(recipe.urls, cleanUrls),
                    ingredients: convertIngredients(recipe.ingredients.trim()),
                    directions: convertDirections(recipe.directions.trim()),
                    categories: recipe.categories,
                };
                if (recipeObj.id > 0) {
                    updateRecipe(recipeObj);
                } else {
                    createRecipe(recipeObj);
                }
            }).catch((ex: any) => console.error(ex));
        }
        ev.preventDefault();
        ev.stopPropagation();
    };
    changeHandler = (ev: React.ChangeEvent<HTMLInputElement|HTMLTextAreaElement>) => {
        const {name} = ev.target;
        const value = ev.target.value;
        // @ts-ignore
        this.setState({
            [name]: value,
        });
    };
    cleanUrlsChangeHandler = (ev: React.ChangeEvent, cleanUrls: boolean) => {
        this.setState({cleanUrls});
    };
    urlPasteHandler = () => {
        const {cleanUrls} = this.state;
        if (cleanUrls) {
            setTimeout(() => {
                const {recipe} = this.state;
                recipe.urls = joinLines(convertUrls(recipe.urls, true));
                this.setState({recipe});
            }, 20);
        }
    };
    cleanPhotoUrlsChangeHandler = (ev: React.ChangeEvent, cleanPhotoUrls: boolean) => {
        this.setState({cleanPhotoUrls});
    };
    photoUrlPasteHandler = () => {
        const {cleanPhotoUrls} = this.state;
        if (cleanPhotoUrls) {
            setTimeout(() => {
                const {recipe} = this.state;
                recipe.photos = joinLines(convertUrls(recipe.photos, true));
                this.setState({recipe});
            }, 20);
        }
    };
    toggleInput = (ev: React.SyntheticEvent<HTMLElement>) => {
        const el = ev.target as HTMLElement;
        const target: string = el.dataset.target || "";
        const targetEl = document.getElementById(target);
        if (targetEl) {
            if (el.classList.contains("collapsed")) {
                el.classList.remove("collapsed");
                targetEl.classList.remove("collapsed");
            } else {
                el.classList.add("collapsed");
                targetEl.classList.add("collapsed");
            }
        }
    };
    categoriesChanged = (categories: Array<string>) => {
        const {recipe} = this.state;
        recipe.categories = categories;
        this.setState({recipe});
    };
    render() {
        const {recipe, editMode} = this.state;
        let header = (recipe.id > 0 ? "Edit Recipe" : "Add New Recipe");
        const attrs: any = { className: 'form-input' };
        let formClasses = "";
        if (!editMode) {
            attrs.readonly = true;
            attrs.disabled = true;
            formClasses = "display-mode";
        }
        return (
            <form data-recipe-id={recipe.id} className={`recipe-form ${formClasses}`}>
                {!editMode && (
                    <div className="form-header button-header">
                        <BackButton />
                        <button className="btn btn-primary" onClick={this.editHandler}>Edit Recipe</button>
                    </div>
                )}
                {editMode && (
                    <h3 className="form-header">
                        <div className="header-text">{header}</div>
                        <button className="btn btn-link" onClick={this.cancelEditHandler}>Cancel</button>
                    </h3>
                )}
                <label htmlFor="recipeName" className="recipe-label required">Name</label>
                <input
                    id="recipeName"
                    name="name"
                    type="text"
                    value={recipe.name}
                    onChange={this.changeHandler}
                    {...attrs} />

                <div className="label-bar">
                    <label htmlFor="description" className="recipe-label">Description</label>
                    <span className="collapse-icon" data-target="description" onClick={this.toggleInput}/>
                </div>
                <textarea
                    id="description"
                    name="description"
                    value={recipe.description}
                    onChange={this.changeHandler}
                    {...attrs}
                />

                <label className="recipe-label" htmlFor="recipeCategories">Categories</label>
                {!editMode && <span className="recipe-categories">{recipe.categories.join(", ")}</span>}
                {editMode && (
                    <CategoryMultiSelect
                        onChange={this.categoriesChanged}
                        className="recipe-categories"
                        initialValue={recipe.categories}
                    />
                )}

                <div className="label-bar">
                    <label htmlFor="recipeUrls" className="recipe-label">Urls</label>
                    {editMode && <CheckBox label="Clean urls" checked onChange={this.cleanUrlsChangeHandler} />}
                </div>
                <textarea
                    id="recipeUrls"
                    name="urls"
                    value={recipe.urls}
                    onChange={this.changeHandler}
                    onPaste={this.urlPasteHandler}
                    {...attrs}
                />

                <div className="label-bar">
                    <label htmlFor="recipePhotos" className="recipe-label">Photos</label>
                    {editMode && <CheckBox label="Clean urls" checked onChange={this.cleanPhotoUrlsChangeHandler} />}
                </div>
                <textarea
                    id="recipePhotos"
                    name="photos"
                    value={recipe.photos}
                    onChange={this.changeHandler}
                    onPaste={this.photoUrlPasteHandler}
                    {...attrs}
                />

                <div className="label-bar">
                    <label htmlFor="ingredients" className="recipe-label">Ingredients</label>
                    <span className="collapse-icon" data-target="ingredients" onClick={this.toggleInput}/>
                </div>
                <textarea
                    id="ingredients"
                    name="ingredients"
                    value={recipe.ingredients}
                    onChange={this.changeHandler}
                    {...attrs}
                />

                <div className="label-bar">
                    <label htmlFor="directions" className="recipe-label">Directions</label>
                    <span className="collapse-icon" data-target="directions" onClick={this.toggleInput}/>
                </div>
                <textarea id="directions" name="directions" value={recipe.directions} {...attrs} />

                <div className="recipe-button-bar">
                    {editMode && <button className="btn" onClick={this.cancelEditHandler}>Cancel</button>}
                    {!editMode && <BackButton />}
                    {editMode && <button className="btn btn-primary" onClick={this.saveHandler}>Save</button>}
                    {!editMode && <button className="btn btn-primary" onClick={this.editHandler}>Edit Recipe</button>}
                </div>
            </form>
        );
    }
}

export default withRouter(NewForm);