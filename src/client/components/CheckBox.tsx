import React from "react";
import './checkbox.scss';

interface CheckBoxProps {
    label: string;
    id?: string;
    disabled?: boolean;
    checked?: boolean;
    onChange?: Function;
}

interface CheckBoxState {
    checked: boolean;
}

let cbIds = 0;

export default class CheckBox extends React.Component<CheckBoxProps, CheckBoxState> {
    cbId: string = "";
    constructor(props: CheckBoxProps) {
        super(props);
        const {id, checked = false} = props;
        this.cbId = (id ? id : `CheckBox_${++cbIds}`);
        this.state = {
            checked,
        };
    }
    changeHandler = (ev: React.ChangeEvent) => {
        const checked = !this.state.checked;
        this.setState({checked});
        const {onChange} = this.props;
        if (onChange) {
            onChange.call(this, ev, checked);
        }
    };
    render() {
        const {label, disabled = false} = this.props;
        const {checked} = this.state;
        const cbAttrs = {checked, disabled};
        return (
            <div className="checkbox-container">
                <input type="checkbox" id={this.cbId} {...cbAttrs} onChange={this.changeHandler}/>
                <label htmlFor={this.cbId} className={`checkbox-label ${disabled ? 'disabled': ''}`}>{label}</label>
            </div>
        )
    }
}