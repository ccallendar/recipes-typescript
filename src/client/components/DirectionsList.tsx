import React from "react";
import {Direction} from "../../imports/model/direction";
import DirectionItem from "./DirectionItem";

type DirectionsListProps = {
    directions?: Array<Direction>;
    condensed?: boolean;
}

export default class DirectionsList extends React.PureComponent<DirectionsListProps> {
    render() {
        const {directions, condensed} = this.props;
        if (!directions || !directions.length) {
            return null;
        }
        return (
            <div className="recipe-directions">
                <h5>Directions</h5>
                <ol className={`directions-list ${condensed ? 'condensed' : ''}`}>
                    {directions.map(direction => <DirectionItem direction={direction} key={direction.id} />)}
                </ol>
            </div>
        )
    }
}