import { EventEmitter } from 'events';
import dispatcher from '../dispatcher';

class CategoryStore extends EventEmitter {
    categories: Array<string>;

    constructor() {
        super();
        this.categories = [];
    }

    load(): void {
        console.log("Loading...");
        this.emit("loading");
        fetch("/api/categories")
            .then(res => res.json())
            .then((categories: Array<string>) => {
                console.log("Categories", categories);
                this.categories = categories;
                this.emit("change");
            }).catch((err) => {
                console.error(err);
            });
    }

    getAll(): Array<string> {
        return this.categories;
    }

    handleActions(action: { type: string }) {
        console.log("handle category action", action);
        if (action.type === "RELOAD_CATEGORIES") {
            this.load();
        }
    }
}

const categoryStore = new CategoryStore();

dispatcher.register(categoryStore.handleActions.bind(categoryStore));

export default categoryStore;
