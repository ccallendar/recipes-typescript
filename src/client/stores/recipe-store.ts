import { EventEmitter } from 'events';
import {Recipe} from '../../imports/model/recipe';
import dispatcher from '../dispatcher';

function post(url: string, body: Object): Promise<any> {
    return fetch(url, {  
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body),
        })
        .then((res: Response) => {
            return res.json();
        }).catch((err: Error) => {
            console.error(err);
            return err;
        });
}

function put(url: string, body: Object): Promise<any> {
    return fetch(url, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body),
        })
        .then((res: Response) => {
            return res.json();
        }).catch((err: Error) => {
            console.error(err);
            return err;
        });
}

function get(url: string): Promise<any> {
    return fetch(url)
        .then(res => res.json())
        .catch((err) => {
            console.error(err);
            return err;
        });
}

class RecipeStore extends EventEmitter {
    recipes: Recipe[];
    constructor() {
        super();
        this.recipes = [];
    }

    load(): void {
        console.log("Loading...");
        this.emit("loading");
        get("/api/recipes")
        .then((res: any) => {
            const recipes:Recipe[] = (res as Recipe[]);
            console.log("Recipes", recipes);
            this.recipes = recipes;
            this.emit("change");
        }).catch((err) => {
            console.error(err);
            return err;
        });
    }

    getAll(): Recipe[] {
        return this.recipes;
    }

    get(id: Number): Recipe|undefined {
        return this.recipes.find((recipe: Recipe) => (recipe.id === id));
    }

    addRecipe(recipe: Recipe): void {
        if (recipe && recipe.name) {
            console.log("Saving new recipe", recipe);
            post(`/api/recipe`, recipe).then((newRecipe: Recipe) => {
                console.log("Insert result", newRecipe);
                this.recipes.push(newRecipe);
                this.emit("change");
                this.emit("added", newRecipe);
            });
        }
    }

    updateRecipe(recipe: Recipe): void {
        if (recipe && recipe.name && recipe.id) {
            console.log("Updating recipe", recipe);
            put(`/api/recipe/${recipe.id}`, recipe).then((updatedRecipe: Recipe) => {
                console.log("Update result", updatedRecipe);
                for (let i = 0; i < this.recipes.length; i++) {
                    if (this.recipes[i].id === updatedRecipe.id) {
                        this.recipes[i] = updatedRecipe;
                        console.log("Updated recipe at ", i);
                        this.emit("change");
                        break;
                    }
                }
            });
        }
    }

    handleActions(action: { type: string; recipe: Recipe }) {
        console.log("handle action", action);
        if (action.type === "RELOAD_RECIPES") {
            this.load();
        } else if (action.type === "CREATE_RECIPE") {
            this.addRecipe(action.recipe);
        } else if (action.type === "UPDATE_RECIPE") {
            this.updateRecipe(action.recipe);
        }
    }
}

const recipeStore = new RecipeStore();

dispatcher.register(recipeStore.handleActions.bind(recipeStore));

export default recipeStore;
