import {splitLines, removeDuplicates, removeQueryString} from "./StringUtils";
import {IIngredient} from "../models/ingredient";
import {IDirection} from "../models/direction";

export default class ModelUtils {
    static convertUrls(str: string, removeQuery: boolean = true):string[] {
        let urls = splitLines(str, false, "http");
        if (removeQuery) {
            urls = urls.map(line => removeQueryString(line));
        }
        return removeDuplicates(urls);
    }

    static convertIngredients(str: string):IIngredient[] {
        const ingredients: IIngredient[] = splitLines(str).map((line): IIngredient => {
            // TODO extract amounts/units
            return {name: line};
        });
        return ingredients;
    }

    static convertDirections(str: string): IDirection[] {
        const directions: IDirection[] = splitLines(str).map((line: string): IDirection => {
            return {direction: line};
        });
        return directions;
    }
}

export const {convertUrls, convertIngredients, convertDirections} = ModelUtils;