export default class StringUtils {

    static joinLines(lines: Array<any>|undefined, property: string = "", separator: string = "\n"): string {
        if (lines && lines.length) {
            return lines.map((line) => {
                if (property && (property in line)) {
                    return line[property];
                }
                return String(line);
            }).filter(Boolean).join(separator);
        }
        return "";
    }

    static splitLines(str: string, includeEmptyLines = false, startsWith: string = ""): Array<string> {
        return (str || "").split(/\s*\r?\n\s*/).map((line: string) => {
            if (line || (!line && includeEmptyLines)) {
                if (startsWith && !line.startsWith(startsWith)) {
                    return "";
                }
                return line;
            }
            return "";
        }).filter(Boolean);
    }

    /**
     * Removes all duplicate items in the array.
     * @param {Array<string>} lines the items to check
     * @param {boolean} [trim=true] if true the string will be trimmed
     * @param {boolean} [ignoreCase=false] if false then case is ignored, but the original string is still used
     */
    static removeDuplicates(lines: string[], trim: boolean = true, ignoreCase: boolean = false): string[] {
        const uniques = new Array<string>();
        const seen = new Set<string>();
        (lines || []).forEach((line) => {
            if (trim) {
                line = line.trim();
            }
            let key = line;
            if (ignoreCase) {
                key = line.toLowerCase();
            }
            if (!seen.has(key)) {
                seen.add(key);
                uniques.push(line);
            }
        });
        return uniques;
    }

    /**
     * Removes the first found question mark and everything after it in the string.
     * @param {string} url
     * @return {string} the url without any query string
     */
    static removeQueryString(url: string): string {
        let str = url || "";
        const qm: number = str.indexOf("?");
        if (qm !== -1) {
            str = str.substring(0, qm);
        }
        return str;
    }
}

export const {joinLines, splitLines, removeQueryString, removeDuplicates} = StringUtils;