import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Recipe} from "./recipe";
import {IDirection} from "./direction";

export interface IComment {
    id?: number;
    comment: string;
    date?: Date;
}

@Entity({ name: 'comments' })
export class Comment implements IComment {
    @PrimaryGeneratedColumn()
    public id: number;

    @ManyToOne(() => Recipe, (recipe) => recipe.id)
    public recipe: Recipe;

    @Column()
    public comment: string;

    @Column("timestamp")
    public date: Date;
}