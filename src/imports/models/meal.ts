import {Rating} from "./rating";
import {Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Recipe} from "./recipe";

/**
 * @swagger
 *
 * definitions:
 *   MealType:
 *     type: string
 *     enum: ['breakfast', 'brunch', 'lunch', 'dinner', 'snack', 'dessert']
 */
export enum MealType {
    breakfast = "breakfast",
    brunch = "brunch",
    lunch = "lunch",
    snack = "snack",
    dinner = "dinner",
    dessert = "dessert",
}

export interface IMeal {
    id: number;
    date: Date;
    dateAdded?: Date;
    type?: MealType;
    notes?: string;
}

export const allMealRelations: string[] = ["recipe", "rating"];

@Entity({ name: 'meals' })
export class Meal implements IMeal {
    @PrimaryGeneratedColumn()
    public id: number;

    @ManyToOne(() => Recipe, (recipe) => recipe.id)
    public recipe: Recipe | undefined;

    @Column("date")
    public date: Date;

    @Column("timestamp")
    public dateAdded: Date;

    @Column({ type: "text", nullable: true })
    notes: string;

    @OneToOne(() => Rating, (rating) => rating.meal)
    rating: Rating | undefined;
}