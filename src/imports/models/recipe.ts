import {Ingredient} from "./ingredient";
import {Direction} from "./direction";
import {Rating} from "./rating";
import {Photo} from "./photo";
import {Comment} from "./comment";
import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Url} from "./url";
import {Category} from "./category";
import {Meal} from "./meal";

export interface IRecipe {
    id: number;
    name: string;
    browserName?: string;
    dateCreated?: Date;
    dateUpdated?: Date;
    description?: string;
    ingredients?: Ingredient[];
    comments?: Comment[];
    directions?: Direction[];
    photos?: Photo[];
    rating?: number;
    ratings?: Rating[];
    urls?: Url[];
    categories?: Category[];
    prepTime?: number;
    cookTime?: number;
    favorite?: boolean;
}

export const allRecipeRelations: string[] = ["directions", "ingredients", "photos", "urls"];

@Entity({ name: 'recipes' })
export class Recipe implements IRecipe {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column({ length: 64, nullable: true })
    public browserName: string;

    @Column("timestamp")
    public dateCreated: Date;

    @Column("timestamp")
    public dateUpdated: Date;

    @Column({ nullable: true })
    description: string;

    rating: number;

    @OneToMany(() => Rating, (rating) => rating.recipe)
    ratings: Rating[];

    @OneToMany(() => Ingredient, (ingredient) => ingredient.recipe)
    ingredients: Ingredient[];

    @OneToMany(() => Direction, (direction) => direction.recipe)
    directions: Direction[];

    @OneToMany(() => Comment, (comment) => comment.recipe)
    comments: Comment[];

    @OneToMany(() => Photo, (photo) => photo.recipe)
    photos: Photo[];

    @OneToMany(() => Url, (url) => url.recipe)
    urls: Url[];

    @OneToMany(() => Category, (category) => category.recipe)
    categories: Category[];

    @OneToMany(() => Meal, (meal) => meal.recipe)
    meals: Meal[];

    @Column({ type: "int", nullable: true })
    prepTime?: number;

    @Column({ type: "int", nullable: true })
    cookTime?: number;

    @Column("boolean")
    favorite?: boolean;

}