import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Recipe} from "./recipe";

export interface IDirection {
    id?: number;
    direction: string;
}

@Entity({ name: 'directions' })
export class Direction implements IDirection {
    @PrimaryGeneratedColumn()
    public id: number;

    @ManyToOne(() => Recipe, (recipe) => recipe.id)
    public recipe: Recipe;

    @Column()
    public direction: string;
}