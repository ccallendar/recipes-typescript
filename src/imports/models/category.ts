import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Recipe} from "./recipe";

export interface ICategory {
    id?: number;
    name: string;
}

@Entity({ name: 'categories' })
export class Category implements ICategory {
    @PrimaryGeneratedColumn()
    public id: number;

    @ManyToOne(() => Recipe, (recipe) => recipe.id)
    public recipe: Recipe;

    @Column()
    public name: string;
}