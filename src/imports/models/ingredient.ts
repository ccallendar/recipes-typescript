import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Recipe} from "./recipe";

export interface IIngredient {
    id?: number;
    amount?: number;
    units?: string;
    name: string;
}

@Entity({ name: 'ingredients' })
export class Ingredient implements IIngredient {
    @PrimaryGeneratedColumn()
    public id: number;

    @ManyToOne(() => Recipe, (recipe) => recipe.id)
    public recipe: Recipe;

    @Column({ type: "real", nullable: true })
    public amount: number;

    @Column({ nullable: true })
    public units: string;

    @Column()
    public name: string;
}