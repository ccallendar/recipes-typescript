import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Recipe} from "./recipe";

export interface IPhoto {
    id?: number;
    url: string;
    alt?: string;
    width?: number;
    height?: number;
    isPrimary?: boolean;
}

@Entity({ name: 'photos' })
export class Photo implements IPhoto {
    @PrimaryGeneratedColumn()
    public id: number;

    @ManyToOne(() => Recipe, (recipe) => recipe.id)
    public recipe: Recipe;

    @Column("text")
    public url: string;

    @Column({ length: 64, nullable: true })
    public alt?: string;

    @Column({ type: "smallint", nullable: true })
    public width?: number;

    @Column({ type: "smallint", nullable: true })
    public height?: number;

    @Column("boolean")
    public isPrimary: boolean;
}