import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Recipe} from "./recipe";

export interface IUrl {
    id?: number;
    url: string;
}

@Entity({ name: 'urls' })
export class Url implements IUrl {
    @PrimaryGeneratedColumn()
    public id: number;

    @ManyToOne(() => Recipe, (recipe) => recipe.id)
    public recipe: Recipe;

    @Column()
    public url: string;
}