import {Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Recipe} from "./recipe";
import {Meal} from "./meal";

export interface IRating {
    id?: number;
    recipeId: number;
    mealId?: number;
    rating: number;
    comment?: string;
    date?: Date;
}

@Entity({ name: 'ratings' })
export class Rating implements IRating {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column("int")
    public recipeId: number;

    @Column({ type: "int", nullable: true })
    public mealId: number | undefined;

    @ManyToOne(() => Recipe, (recipe) => recipe.id)
    public recipe: Recipe;

    @OneToOne(() => Meal, (meal) => meal.rating)
    @JoinColumn()
    public meal: Meal;

    @Column("int")
    public rating: number;

    @Column({ type: "text", nullable: true })
    public comment: string;

    @Column("timestamp")
    public date: Date;

}