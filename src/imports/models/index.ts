import {Category} from "./category";
import {Comment} from "./comment";
import {Direction} from "./direction";
import {Ingredient} from "./ingredient";
import {Meal} from "./meal";
import {Photo} from "./photo";
import {Rating} from "./rating";
import {Recipe} from "./recipe";
import {Url} from "./url";

const entities = [Category, Comment, Direction, Ingredient, Meal, Photo, Rating, Recipe, Url];

export default entities;
