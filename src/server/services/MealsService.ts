import {IMealSearchParams, ISearchResult} from "./interfaces";
import {FindManyOptions, getRepository} from "typeorm";
import {BadRequestError, InternalServerError, NotFoundError} from "../errors";
import {IMeal, Meal} from "../../imports/models/meal";
import BaseService from "./BaseService";
import {IRecipe, Recipe} from "../../imports/models/recipe";


export default class MealsService extends BaseService {

    public async getAllMeals(params: IMealSearchParams): Promise<ISearchResult<IMeal>> {
        const options: FindManyOptions<Meal> = this.getFindManyOptions(params);
        if (params.order) {
            options.order = params.order;
        }
        try {
            const [items, count]: [Meal[], number] = await getRepository(Meal).findAndCount(options);
            return {items, count} as ISearchResult<IMeal>;
        } catch (err) {
            console.error(err);
            throw new InternalServerError(err.message || "Failed to find meals");
        }
    }

    public async getMealById(id: number, relations?: string[]): Promise<IMeal|undefined> {
        let meal: Meal|undefined;
        try {
            meal = await getRepository(Meal).findOne({
                where: {id},
                relations,
            });
        } catch (err) {
            console.error(err);
            throw new InternalServerError(err.message || "Failed to get meal by id");
        }
        if (meal) {
            return meal;
        } else {
            throw new NotFoundError(`Couldn't find meal with id ${id}`);
        }
    }

    public async upsertMeal(meal: IMeal): Promise<IMeal> {
        try {
            return await getRepository(Meal).save(meal);
        } catch (err) {
            throw new InternalServerError(err.message);
        }
    }
}