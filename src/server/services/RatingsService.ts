import {getRepository} from "typeorm";
import {BadRequestError, InternalServerError, NotFoundError} from "../errors";
import BaseService from "./BaseService";
import {IRating, Rating} from "../../imports/models/rating";
import {IRecipe, Recipe} from "../../imports/models/recipe";


export default class RatingsService extends BaseService {

    public async getRatingById(id: number): Promise<IRating|undefined> {
        let rating: Rating|undefined;
        try {
            rating = await getRepository(Rating).findOne({
                where: {id},
            });
        } catch (err) {
            console.error(err);
            throw new InternalServerError(err.message || "Failed to get rating by id");
        }
        if (rating) {
            return rating;
        } else {
            throw new NotFoundError(`Couldn't find rating with id ${id}`);
        }
    }

    public async getRatingsForRecipe(recipeId: number): Promise<IRating[]> {
        let ratings: IRating[] = [];
        try {
            ratings = await getRepository(Rating).find({
                where: {recipeId},
            });
        } catch (err) {
            console.error(err);
            throw new InternalServerError(err.message || "Failed to get ratings by recipe id");
        }
        if (ratings) {
            return ratings;
        } else {
            throw new NotFoundError(`Couldn't find ratings for recipe id ${recipeId}`);
        }
    }

    public async insertRating(rating: IRating): Promise<IRating> {
        try {
            return await getRepository(Rating).save(rating);
        } catch (err) {
            throw new InternalServerError(err.message);
        }
    }

}