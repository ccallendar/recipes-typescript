import {FindManyOptions} from "typeorm";
import {ISearchParams} from "./interfaces";

export default class BaseService {
    protected getFindManyOptions(params: ISearchParams): FindManyOptions {
        const page = Math.max(1, params.page);
        const size = Math.max(1, params.size);
        return {
            skip: (page - 1) * params.size,
            take: size,
            relations: params.relations,
        };
    }
}