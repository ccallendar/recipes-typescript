import {IRecipe} from "../../imports/models/recipe";
import {IMeal} from "../../imports/models/meal";

export interface ISearchParams {
    key?: string;
    page: number;
    size: number;
    relations?: string[];
}

export interface ISearchResult<T> {
    items: T[];
    count: number;
}

export interface ILink {
    prev?: string;
    next?: string;
}

export interface IRecipeSearchParams extends ISearchParams {
    order?: {
        [P in keyof IRecipe]?: "ASC" | "DESC" | 1 | -1;
    };
}

export interface IMealSearchParams extends ISearchParams {
    order?: {
        [P in keyof IMeal]?: "ASC" | "DESC" | 1 | -1;
    };
}