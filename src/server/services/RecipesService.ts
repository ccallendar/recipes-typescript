import {createQueryBuilder, FindManyOptions, getRepository} from "typeorm";
import {IRecipe, Recipe} from "../../imports/models/recipe";
import {IRecipeSearchParams, ISearchParams, ISearchResult} from "./interfaces";
import {Category, ICategory} from "../../imports/models/category";
import {BadRequestError, InternalServerError, NotFoundError} from "../errors";
import BaseService from "./BaseService";


export default class RecipesService extends BaseService {

    public async getCategories(): Promise<ICategory[]> {
        try {
            return await getRepository(Category).find();
        } catch (err) {
            throw new InternalServerError(err.message);
        }
    }

    public async getAllRecipes(params: IRecipeSearchParams): Promise<ISearchResult<IRecipe>> {
        const options: FindManyOptions<Recipe> = this.getFindManyOptions(params);
        if (params.order) {
            options.order = params.order;
        }
        try {
            const [items, count]: [Recipe[], number] = await getRepository(Recipe).findAndCount(options);
            return {items, count} as ISearchResult<IRecipe>;
        } catch (err) {
            console.error(err);
            throw new InternalServerError(err.message || "Failed to find recipes");
        }
    }

    public async getRecipeById(id: number, relations?: string[]): Promise<IRecipe|undefined> {
        let recipe: Recipe|undefined;
        try {
            recipe = await getRepository(Recipe).findOne({
                where: {id},
                relations,
            });
        } catch (err) {
            console.error(err);
            throw new InternalServerError(err.message || "Failed to get recipe by id");
        }
        if (recipe) {
            return recipe;
        } else {
            throw new NotFoundError(`Couldn't find recipe with id ${id}`);
        }
    }

    public async searchRecipes(params: ISearchParams): Promise<ISearchResult<IRecipe>> {
        const {page, size} = params;
        let where = "";
        let whereParams = {};
        if (params.key) {
            where = " (name ILIKE :key OR" +
                "  description ILIKE :key)";
            whereParams = {...whereParams, ...{key: `%${params.key}%`}};
        }
        try {
            const [items, count] = await createQueryBuilder(Recipe)
                .where(where, whereParams)
                .skip((page - 1) * size)
                .take(size)
                .getManyAndCount();
            return {items, count} as ISearchResult<IRecipe>;
        } catch (err) {
            console.error(err);
            throw new InternalServerError(err.message || "Failed to search recipes");
        }
    }

    public async checkIfBrowserNameExists(browserName: string):Promise<boolean> {
        // console.log(`Checking if browser name exists '${browserName}'`);
        const recipe: Recipe|undefined = await getRepository(Recipe).findOne({ where: { browserName }});
        console.log(" -> Check result", recipe);
        return (recipe !== undefined);
    }

    public static makeBrowserName(name: string):string {
        return (name || "").toLowerCase().replace(/\s+/g, "-");
    }

    public async upsertRecipe(recipe: IRecipe): Promise<IRecipe> {
        if (!recipe.name) {
            throw new BadRequestError("Recipe must have a name");
        }
        recipe.browserName = RecipesService.makeBrowserName(recipe.browserName || recipe.name);
        let counter = 1;
        const browserNameBase = recipe.browserName;
        while (await this.checkIfBrowserNameExists(recipe.browserName)) {
            recipe.browserName = `${browserNameBase}-${++counter}`;
            if (counter > 10) {
                recipe.browserName = `${browserNameBase}-${Date.now()}`;
                break;
            }
        }
        try {
            return await getRepository(Recipe).save(recipe);
        } catch (err) {
            throw new InternalServerError(err.message);
        }
    }

}