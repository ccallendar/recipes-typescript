import {Recipe} from '../../imports/model/recipe';

// const {Pool/*, Client*/} = require("pg");
import {Pool, QueryResult} from 'pg';
import {Photo} from "../../imports/model/photo";
import {Ingredient} from "../../imports/model/ingredient";
import {Direction} from "../../imports/model/direction";

// const dbUrl = "postgres://recipemaster:recipemaster@localhost/recipes";
// console.log("Connecting...");
// client.connect();
// client.query('SELECT NOW()', (err, res) => {
//   // console.log(err, res)
//   client.end()
// });

export default class Database {
  client = new Pool({
    host: 'localhost',
    database: 'recipes',
    user: 'recipemaster',
    password: 'recipemaster',
    port: 5432,
  });

  static convertDboToModel(obj: any, fieldsToIgnore: Array<string> = []): any {
    const model: any = {};
    if (obj) {
      Object.keys(obj).forEach((key) => {
        if (obj.hasOwnProperty(key) && (fieldsToIgnore.indexOf(key) === -1)) {
          const camelCase = key.split(/_+/g).map((str, idx) => {
            if ((idx > 0) && str) {
              return str[0].toUpperCase() + str.substr(1);
            }
            return str;
          }).join("");
          model[camelCase] = obj[key];
        }
      });
    }
    return model;
  }

  async convertRecipe(obj: any): Promise<Recipe> {
    const {id} = obj;
    const recipe = Database.convertDboToModel(obj) as Recipe;
    return Promise.all([
      this.getRecipeUrls(id).then(urls => recipe.urls = (urls.length ? urls : undefined)),
      //this.getRecipeComments(id).then(comments => recipe.comments = comments),
      this.getRecipePhotos(id).then(photos => recipe.photos = (photos.length ? photos : undefined)),
      this.getRecipeDirections(id).then(directions => recipe.directions = (directions.length ? directions : undefined)),
      this.getRecipeIngredients(id).then(ingredients => recipe.ingredients = (ingredients.length ? ingredients : undefined)),
      this.getRecipeCategories(id).then(categories => recipe.categories = (categories.length ? categories : undefined)),
      // this.getRecipeRatings(id).then(ratings => recipe.ratings = ratings),
    ]).then(() => {
      return recipe;
    });
    // recipe.urls = await this.getRecipeUrls(id);
    // recipe.comments = await this.getRecipeComments(id);
    // recipe.photos = await this.getRecipePhotos(id);
    // recipe.directions = await this.getRecipeDirections(id);
    // recipe.ingredients = await this.getRecipeIngredients(id);
    // recipe.categories = await this.getRecipeCategories(id);
    // recipe.ratings = await this.getRecipeRatings(id);
  }

  async getRecipeUrls(id: number):Promise<Array<string>> {
    const result = await this.client.query(`SELECT * FROM urls WHERE recipe_id = ${id}`);
    // console.log("Urls result", result.rows);
    return result.rows.map(row => row.url);
  }

  async getRecipePhotos(id: number):Promise<Array<Photo>> {
    const result = await this.client.query(`SELECT * FROM photos WHERE recipe_id = ${id}`);
    // console.log("Photos result", result.rows);
    return result.rows.map((row) => {
      const {url, alt, width, height, is_primary = false} = row;
      return {url, alt, width, height, isPrimary: is_primary};
    });
  }

  async getRecipeDirections(id: number):Promise<Array<Direction>> {
    const result = await this.client.query(`SELECT * FROM directions WHERE recipe_id = ${id}`);
    // console.log("Directions result", result.rows);
    return result.rows.map((row) => {
      const {direction} = row;
      return {direction};
    });
  }

  async getRecipeIngredients(id: number):Promise<Array<Ingredient>> {
    const result = await this.client.query(`SELECT * FROM ingredients WHERE recipe_id = ${id}`);
    // console.log("Ingredients result", result.rows);
    return result.rows.map((row) => {
      const {amount, units, name} = row;
      return {amount, units, name};
    });
  }

  async getRecipeCategories(id: number):Promise<Array<string>> {
    const result = await this.client.query(`SELECT name FROM categories WHERE recipe_id = ${id}`);
    return result.rows.map(row => row.name);
  }

  async getCategories():Promise<Array<string>> {
    const result = await this.client.query("SELECT DISTINCT name FROM categories ORDER BY name");
    return result.rows.map(row => row.name);
  }

  async getRecipesCount():Promise<number> {
    const result = await this.client.query("SELECT COUNT(*) FROM recipes");
    console.log("getRecipesCount", result.rows);
    return Number(result.rows[0].count);
  }

  async getAllRecipes():Promise<Array<Recipe>> {
    // console.log("Getting all recipes...");
    const result = await this.client.query("SELECT * FROM recipes");
    // console.log("Recipes result", result.rows);
    const recipes:Array<Recipe> = [];
    for (let i = 0; i < result.rows.length; i++) {
      const row = result.rows[i];
      console.log(row);
      const recipe: Recipe = await this.convertRecipe(row);
      recipes.push(recipe);
    }
    // console.log("Recipes", recipes);
    return recipes;
  }

  async getRecipe(id: number):Promise<Recipe|null> {
    // console.log("Getting recipe...", id);
    const result = await this.client.query(`SELECT * FROM recipes WHERE id = ${id}`);
    // console.log("Recipes result", result.rows);
    if (result.rowCount === 1) {
      const row = result.rows[0];
      console.log(row);
      const recipe: Recipe = await this.convertRecipe(row);
      console.log("Recipe", recipe);
      return recipe;
    }
    console.warn("Couldn't find recipe with id " + id);
    return null;
  }

  async checkIfBrowserNameExists(browserName: string):Promise<boolean> {
    const sql = "SELECT id FROM recipes WHERE browser_name = $1";
    // console.log(`Checking if browser name exists '${browserName}'`);
    const result: QueryResult = await this.client.query(sql,[browserName]);
    // console.log(" -> Check result", result.rowCount);
    return (result.rowCount > 0);
  }

  static makeBrowserName(name: string):string {
    return (name || "").toLowerCase().replace(/\s+/g, "-");
  }

  async upsertRecipe(recipe: Recipe): Promise<number> {
    if (!recipe.name) {
      throw new Error("Recipe must have a name");
    }
    recipe.browserName = Database.makeBrowserName(recipe.browserName || recipe.name);
    let counter = 1;
    const browserNameBase = recipe.browserName;
    while (await this.checkIfBrowserNameExists(recipe.browserName)) {
      recipe.browserName = `${browserNameBase}-${++counter}`;
      if (counter > 20) {
        break;
      }
    }

    const {id, name, browserName, description, prepTime, cookTime, favorite} = recipe;
    let recipeId = -1;
    let result;
    if (id > 0) {
      recipeId = id;
      console.log("Updating recipe", recipe);
      const sql = "UPDATE recipes" +
          " SET name = $1, browser_name = $2, date_updated = now(), description = $3" +
          " prep_time = $4, cook_time = $5, favorite = $6" +
          " WHERE id = $7";
      const params = [name, browserName, description || "", prepTime, cookTime, !!favorite, id];
      result = await this.client.query(sql, params);
    } else {
      console.log("Inserting new recipe", recipe);
      const sql = "INSERT INTO recipes(name, browser_name, date_created, date_updated, description, prep_time, cook_time, favorite)" +
          " VALUES($1, $2, now(), now(), $3, $4, $5, $6) RETURNING id";
      const params = [name, browserName, description || "", prepTime, cookTime, favorite];
      result = await this.client.query(sql, params);
      if (result.rowCount === 1) {
        recipeId = result.rows[0].id;
      }
    }
    console.log("Result", result.rowCount, result.rows);
    if (result.rowCount === 1) {
      await this.insertRecipeUrls(recipeId, recipe.urls);
      await this.insertRecipePhotos(recipeId, recipe.photos);
      await this.insertRecipeIngredients(recipeId, recipe.ingredients);
      await this.insertRecipeDirections(recipeId, recipe.directions);
      await this.insertRecipeCategories(recipeId, recipe.categories)
    } else {
      recipeId = -1;
      console.log("Failed to insert/update recipe", id, result);
    }
    return recipeId;
  }

  async insertRecipeUrls(recipeId: number, urls: Array<string>|undefined) {
    if (recipeId) {
      await this.client.query(`DELETE FROM urls WHERE recipe_id = ${recipeId}`);
      if (urls && urls.length) {
        for (let i = 0; i < urls.length; i++) {
          await this.client.query("INSERT INTO urls(recipe_id, url) VALUES($1, $2)",
              [recipeId, urls[i]]);
        }
      }
    }
  }

  async insertRecipePhotos(recipeId: number, photos: Array<Photo>|undefined) {
    if (recipeId) {
      await this.client.query(`DELETE FROM photos WHERE recipe_id = ${recipeId}`);
      if (photos && photos.length) {
        for (let i = 0; i < photos.length; i++) {
          const {url, alt = "", width = 0, height = 0, isPrimary = false} = photos[i];
          await this.client.query("INSERT INTO photos(recipe_id, url, alt, width, height, is_primary) " +
              "VALUES($1, $2, $3, $4, $5, $6)",
              [recipeId, url, alt, Number(width), Number(height), isPrimary]);
        }
      }
    }
  }

  async insertRecipeIngredients(recipeId: number, ingredients: Array<Ingredient>|undefined) {
    if (recipeId) {
      await this.client.query(`DELETE FROM ingredients WHERE recipe_id = ${recipeId}`);
      if (ingredients && ingredients.length) {
        for (let i = 0; i < ingredients.length; i++) {
          let {amount, units, name} = ingredients[i];
          if ((amount === undefined) || isNaN(amount)) {
            amount = undefined;
          }
          await this.client.query("INSERT INTO ingredients(recipe_id, amount, units, name) " +
              "VALUES($1, $2, $3, $4)",
              [recipeId, amount, units || "", name]);
        }
      }
    }
  }

  async insertRecipeDirections(recipeId: number, directions: Array<Direction>|undefined) {
    if (recipeId) {
      await this.client.query(`DELETE FROM directions WHERE recipe_id = ${recipeId}`);
      if (directions && directions.length) {
        for (let i = 0; i < directions.length; i++) {
          const {direction} = directions[i];
          await this.client.query("INSERT INTO directions(recipe_id, direction) " +
              "VALUES($1, $2)",
              [recipeId, direction]);
        }
      }
    }
  }

  async insertRecipeCategories(recipeId: number, categories: Array<string>|undefined) {
    if (recipeId) {
      await this.client.query(`DELETE FROM categories WHERE recipe_id = ${recipeId}`);
      if (categories && categories.length) {
        for (let i = 0; i < categories.length; i++) {
          const name = categories[i];
          await this.client.query("INSERT INTO directions(recipe_id, name) " +
              "VALUES($1, $2)", [recipeId, name]);
        }
      }
    }
  }

}