import { Connection, ConnectionOptions, createConnection, getConnectionOptions } from "typeorm";
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

import entities from "../../imports/models";

export enum ConnectionName {
    DEFAULT = "default",
    TEST = "test",
}

export enum DBErrorCodes {
    ForeignKeyConstraint = "23503",
    UniqueConstraint = "23505",
}

export enum DBErrorNames {
    EntityNotFound = "EntityNotFound",
}

export default class DatabaseConnection {
    private connection: Connection;
    private readonly connectionName: ConnectionName;
    constructor(connectionName: ConnectionName = ConnectionName.DEFAULT) {
        this.connectionName = connectionName;
    }
    public async connect(): Promise<Connection> {
        if (!this.connection) {
            try {
                const options: ConnectionOptions = Object.assign(await getConnectionOptions(this.connectionName), {
                    name: "default",
                    namingStrategy: new SnakeNamingStrategy(),
                    entities,
                });
                console.log(`Connecting to ${options.database}...`);
                this.connection = await createConnection(options);
                console.log("DB connected!");
            } catch (e) {
                console.error("DB connection error:", e);
                throw e;
            }
        }
        return this.connection;
    }
    public async disconnect(): Promise<Connection> {
        if (this.connection) {
            try {
                console.log(`Disconnecting from ${this.connection.options.database}.`);
                await this.connection.close();
            } catch (e) {
                console.error("DB Disconnection error:", e);
                throw e;
            }
        }
        return this.connection;
    }
}
