import BaseController from "./BaseController";
import {Request, Response} from "express";
import {BadRequestError, NotFoundError} from "../errors";
import RatingsService from "../services/RatingsService";
import {IRating} from "../../imports/models/rating";

export default class RecipesController extends BaseController {

    private service: RatingsService;

    constructor(service: RatingsService = new RatingsService()) {
        super();
        this.service = service;
    }

    /**
     * @swagger
     * /api/ratings/{recipeId}:
     *  get:
     *      tags:
     *          - "Ratings"
     *      description: Fetches all the ratings for a given recipe.
     *      parameters:
     *          - $ref: '#/parameters/recipeId'
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: Successfully fetched all ratings
     */
    public async findRatingsForRecipe(req: Request, res: Response): Promise<void> {
        const recipeId = Number(req.params.recipeId);
        if (recipeId > 0) {
            try {
                const ratings: IRating[] = await this.service.getRatingsForRecipe(recipeId);
                res.status(200).json(ratings);
            } catch (err) {
                this.error(err, res);
            }
        } else {
            this.error(new BadRequestError("Invalid Recipe Id"), res);
        }
    }

    /**
     * @swagger
     *
     * /api/rating/{id}:
     *  get:
     *      tags:
     *          - "Ratings"
     *      description: Gets a single rating.
     *      parameters:
     *          - $ref: '#/parameters/id'
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: Successfully fetched rating
     *          400:
     *              description: Invalid request
     *          404:
     *              description: Failed to find rating
     */
    public async findById(req: Request, res: Response): Promise<void> {
        const ratingId = Number(req.params.id);
        if (ratingId > 0) {
            try {
                const rating: IRating|undefined = await this.service.getRatingById(ratingId);
                if (rating) {
                    res.status(200).json(rating);
                } else {
                    this.error(new NotFoundError(`Rating ${ratingId} not found`), res);
                }
            } catch (err) {
                this.error(err, res);
            }
        } else {
            this.error(new BadRequestError("Invalid Rating Id"), res);
        }
    }

    /**
     * @swagger
     *
     * /api/rating:
     *  post:
     *      tags:
     *          - "Ratings"
     *      description: Creates a new rating.
     *      parameters:
     *          - in: body
     *            name: rating
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: Successfully created new rating
     *          400:
     *              description: Failed to create rating
     *              $ref: '#/responses/BadRequest'
     */
    public async create(req: Request, res: Response): Promise<void> {
        console.log("Creating new rating", req.body);
        try {
            let rating: IRating = (req.body as IRating);
            if (rating.id && (rating.id > 0)) {
                this.error(new BadRequestError(`This rating already has an id (${rating.id})`), res);
            } else {
                console.log("Creating new rating", rating);
                rating = await this.service.insertRating(rating);
                if (rating) {
                    res.status(201).json(rating);
                } else {
                    this.error(new NotFoundError("Failed to find created rating"), res);
                }
            }
        } catch (err) {
            this.error(err, res);
        }
    }

}
