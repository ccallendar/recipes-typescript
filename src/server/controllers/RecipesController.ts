import {Request, Response} from "express";
import {allRecipeRelations, IRecipe, Recipe} from "../../imports/models/recipe";
import RecipesService from "../services/RecipesService";
import {IRecipeSearchParams} from "../services/interfaces";
import BaseController from "./BaseController";
import {ICategory} from "../../imports/models/category";
import {BadRequestError, NotFoundError} from "../errors";

export default class RecipesController extends BaseController {

    private service: RecipesService;

    constructor(service: RecipesService = new RecipesService()) {
        super();
        this.service = service;
    }

    /**
     * @swagger
     *
     * /api/categories:
     *  get:
     *      tags:
     *          - "Categories"
     *      description: Gets all recipe categories
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: All recipe categories fetched successfully
     */
    public async getCategories(req: Request, res: Response): Promise<void> {
        try {
            const categories: ICategory[] = await this.service.getCategories();
            const strings: string[] = categories.map(category => category.name);
            res.status(200).json(strings);
        } catch (err) {
            this.error(err, res);
        }
    }

    /**
     * @swagger
     * /api/recipes:
     *  get:
     *      tags:
     *          - "Recipes"
     *      description: Loads all the recipes.
     *      parameters:
     *          - $ref: '#/parameters/page'
     *          - $ref: '#/parameters/size'
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: Successfully fetched all recipes
     */
    public async getAll(req: Request, res: Response): Promise<void> {
        const params: IRecipeSearchParams = this.getRecipeSearchParams(req);
        try {
            const {items, count} = await this.service.getAllRecipes(params);
            this.setPagingResponse(req, res, params, count);
            res.status(200).json(items);
        } catch (err) {
            this.error(err,  res);
        }
    }

    private getRecipeSearchParams(req: Request): IRecipeSearchParams {
        const params: IRecipeSearchParams = this.getSearchParams(req);
        params.relations = allRecipeRelations;
        params.order = {
            name: "ASC"
        };
        return params;
    }

    /**
     * @swagger
     * /api/recipes/search:
     *  get:
     *      tags:
     *          - "Recipes"
     *      description: Searches the recipes.
     *      parameters:
     *          - $ref: '#/parameters/searchQuery'
     *          - $ref: '#/parameters/page'
     *          - $ref: '#/parameters/size'
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: Successfully searched recipes
     */
    public async search(req: Request, res: Response): Promise<void> {
        const params: IRecipeSearchParams = this.getRecipeSearchParams(req);
        try {
            const {items, count} = await this.service.searchRecipes(params);
            this.setPagingResponse(req, res, params, count);
            res.status(200).json(items);
        } catch (err) {
            this.error(err,  res);
        }
    }

    /**
     * @swagger
     *
     * /api/recipe/{recipeId}:
     *  get:
     *      tags:
     *          - "Recipes"
     *      description: Gets a single recipe.
     *      parameters:
     *          - $ref: '#/parameters/recipeId'
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: Successfully fetched recipe
     *          400:
     *              description: Invalid request
     *          404:
     *              description: Failed to find recipe
     */
    public async findById(req: Request, res: Response): Promise<void> {
        const recipeId = Number(req.params.recipeId);
        if (recipeId > 0) {
            try {
                const recipe: IRecipe | undefined = await this.service.getRecipeById(recipeId, allRecipeRelations);
                if (recipe) {
                    res.status(200).json(recipe);
                } else {
                    this.error(new NotFoundError(`Recipe ${recipeId} not found`), res);
                }
            } catch (err) {
                this.error(err, res);
            }
        } else {
            this.error(new BadRequestError("Invalid Recipe Id"), res);
        }
    }

    /**
     * @swagger
     *
     * /api/recipe:
     *  post:
     *      tags:
     *          - "Recipes"
     *      description: Creates a new recipe.
     *      parameters:
     *          - in: body
     *            name: recipe
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: Successfully created new recipe
     *          400:
     *              description: Failed to create recipe
     *              $ref: '#/responses/BadRequest'
     */
    public async create(req: Request, res: Response): Promise<void> {
        console.log("Creating new recipe", req.body);
        try {
            let recipe: IRecipe = (req.body as IRecipe);
            if (recipe.id > 0) {
                this.error(new BadRequestError(`This recipe already has an id (${recipe.id})`), res);
            } else {
                console.log("Creating new recipe", recipe);
                recipe = await this.service.upsertRecipe(recipe);
                if (recipe) {
                    console.log("Created recipe", recipe.id);
                    res.status(201).json(recipe);
                } else {
                    this.error(new NotFoundError("Failed to find created recipe"), res);
                }
            }
        } catch (err) {
            this.error(err, res);
        }
    }

    /**
     * @swagger
     *
     * /api/recipe/{recipeId}:
     *  put:
     *      tags:
     *          - "Recipes"
     *      description: Updates an existing recipe.
     *      parameters:
     *          - $ref: '#/parameters/recipeId'
     *          - in: body
     *            name: recipe
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: Successfully updated recipe
     *          400:
     *              description: Failed to update recipe
     *              $ref: '#/responses/BadRequest'
     */
    public async update(req: Request, res: Response): Promise<void> {
        console.log("Update recipe", req.body, req.params);
        try {
            let recipe: IRecipe = (req.body as IRecipe);
            console.log("Recipe", recipe);
            const recipeId = Number(req.params.recipeId);
            if ((recipeId > 0) && (recipeId === recipe.id)) {
                console.log("Updating recipe", recipe);
                recipe = await this.service.upsertRecipe(recipe);
                if (recipe) {
                    console.log("Updated recipe successfully");
                    res.status(200).json(recipe);
                } else {
                   this.error(new NotFoundError(`Failed to update recipe with id ${recipeId}`), res);
                }
            } else {
                this.error(new BadRequestError("Can't update recipe - invalid id"), res);
            }
        } catch (err) {
            this.error(err, res);
        }
    }

}
