import BaseController from "./BaseController";
import MealsService from "../services/MealsService";
import {IMealSearchParams} from "../services/interfaces";
import {allMealRelations, IMeal} from "../../imports/models/meal";
import {Request, Response} from "express";
import {BadRequestError, NotFoundError} from "../errors";

export default class MealsController extends BaseController {

    private service: MealsService;

    constructor(service: MealsService = new MealsService()) {
        super();
        this.service = service;
    }

    /**
     * @swagger
     * /api/meals:
     *  get:
     *      tags:
     *          - "Meals"
     *      description: Loads all the meals.
     *      parameters:
     *          - $ref: '#/parameters/page'
     *          - $ref: '#/parameters/size'
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: Successfully fetched all meals
     */
    public async getAll(req: Request, res: Response): Promise<void> {
        const params: IMealSearchParams = this.getMealSearchParams(req);
        try {
            const {items, count} = await this.service.getAllMeals(params);
            this.setPagingResponse(req, res, params, count);
            res.status(200).json(items);
        } catch (err) {
            this.error(err,  res);
        }
    }

    private getMealSearchParams(req: Request): IMealSearchParams {
        const params: IMealSearchParams = this.getSearchParams(req);
        params.relations = allMealRelations;
        params.order = {
            date: "ASC"
        };
        return params;
    }

    /**
     * @swagger
     *
     * /api/meal/{mealId}:
     *  get:
     *      tags:
     *          - "Meals"
     *      description: Gets a single recipe.
     *      parameters:
     *          - $ref: '#/parameters/id'
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: Successfully fetched meal
     *          400:
     *              description: Invalid request
     *          404:
     *              description: Failed to find meal
     */
    public async findById(req: Request, res: Response): Promise<void> {
        const mealId = Number(req.params.id);
        if (mealId > 0) {
            try {
                const meal: IMeal|undefined = await this.service.getMealById(mealId, allMealRelations);
                if (meal) {
                    res.status(200).json(meal);
                } else {
                    this.error(new NotFoundError(`Meal ${mealId} not found`), res);
                }
            } catch (err) {
                this.error(err, res);
            }
        } else {
            this.error(new BadRequestError("Invalid Meal Id"), res);
        }
    }

    /**
     * @swagger
     *
     * /api/meal:
     *  post:
     *      tags:
     *          - "Meals"
     *      description: Creates a new meal.
     *      parameters:
     *          - in: body
     *            name: meal
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: Successfully created new meal
     *          400:
     *              description: Failed to create meal
     *              $ref: '#/responses/BadRequest'
     */
    public async create(req: Request, res: Response): Promise<void> {
        console.log("Creating new recipe", req.body);
        try {
            let meal: IMeal = (req.body as IMeal);
            if (meal.id > 0) {
                this.error(new BadRequestError(`This meal already has an id (${meal.id})`), res);
            } else {
                console.log("Creating new meal", meal);
                meal = await this.service.upsertMeal(meal);
                if (meal) {
                    console.log("Created meal", meal.id);
                    res.status(201).json(meal);
                } else {
                    this.error(new NotFoundError("Failed to find created meal"), res);
                }
            }
        } catch (err) {
            this.error(err, res);
        }
    }

    /**
     * @swagger
     *
     * /api/meal/{mealId}:
     *  put:
     *      tags:
     *          - "Meals"
     *      description: Updates an existing meal.
     *      parameters:
     *          - $ref: '#/parameters/id'
     *          - in: body
     *            name: meal
     *      produces:
     *          - application/json
     *      responses:
     *          200:
     *              description: Successfully updated meal
     *          400:
     *              description: Failed to update meal
     *              $ref: '#/responses/BadRequest'
     */
    public async update(req: Request, res: Response): Promise<void> {
        console.log("Update meal", req.body, req.params);
        try {
            let meal: IMeal = (req.body as IMeal);
            const mealId = Number(req.params.id);
            if ((mealId > 0) && (meal.id !== mealId)) {
                console.log("Updating meal", meal);
                meal = await this.service.upsertMeal(meal);
                if (meal) {
                    console.log("Updated meal successfully");
                    res.status(200).json(meal);
                } else {
                   this.error(new NotFoundError(`Failed to update meal with id ${mealId}`), res);
                }
            } else {
                this.error(new BadRequestError("Can't update meal - invalid id"), res);
            }
        } catch (err) {
            this.error(err, res);
        }
    }

}
