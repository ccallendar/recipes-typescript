
export interface IErrorBody {
    success: boolean;
    status: number;
    msg: string;
    error?: Error;
}