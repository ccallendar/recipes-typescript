import {ILink, ISearchParams} from "../services/interfaces";
import {Request, Response} from "express";
import {BaseHttpError} from "../errors";
import {IErrorBody} from "./interfaces";
const url = require("url");

export default class BaseController {

    protected getNumberParam(param: any, defaultValue: number = 0): number {
        if (!param || isNaN(Number(param))) {
            return defaultValue;
        }
        return Number(param);
    }

    protected getSearchParams(req: Request): ISearchParams {
        const params: ISearchParams = {
            page: Math.max(1, this.getNumberParam(req.query.page, 1)),
            size: Math.max(1, this.getNumberParam(req.query.size, 100)),
        };
        try {
            const queryObject = url.parse(req.url, true).query;
            if (queryObject.key !== undefined) {
                params.key = queryObject.key;
            }
        } catch (err) {
            console.error("Failed to parse url", err);
        }
        return params;
    }

    protected error(e: Error|string, res: Response, props: object = {}): void {
        let msg;
        let status = 500;
        let error = undefined;
        if (e instanceof BaseHttpError) {
            ({status} = (e as BaseHttpError));
            error = e;
        } else if (e instanceof Error) {
            msg = e.message;
            error = e;
        } else {
            msg = String(e);
        }
        console.error("Error", msg);
        const body: IErrorBody = {...props, success: false, msg: msg || "Server error", status};
        if (error) body.error = error;
        res.status(status).json(body);
    }

    protected setPagingResponse(req: Request, res: Response, params: ISearchParams, total: number): void {
        const { page, size } = params;
        // Need to allow the X-Total-Count header through for CORS requests
        res.header("Access-Control-Expose-Headers", "X-Total-Count,Links");
        if (page && size && total > 0) {
            res.links(this.links(req, page, size, total));
        }
        res.set("X-Total-Count", String(total));
    }

    protected links(req: Request, page: number, size: number, total: number): ILink {
        const inputUrl = `${req.protocol}://${req.get("host")}${req.originalUrl}`;
        return BaseController.generateLinks(inputUrl, page, size, total);
    }

    /**
     * Generates the prev and next links based on the input url.
     * @param {string} inputUrl the url - protocol://host:port/path?query
     * @param {number} page the page number
     * @param {number} size the number of records per page
     * @param {number} total the total number of unfiltered records
     */
    public static generateLinks(inputUrl: string, page: number, size: number, total: number): ILink {
        const links: ILink = {};
        const urlObj = new URL(inputUrl);
        if (page) {
            if (page > 1) {
                urlObj.searchParams.set("page", (page - 1).toString());
                links.prev = urlObj.toString();
            }
            if (size) {
                urlObj.searchParams.set("size", size.toString());
                if (page > 0 && size > 0 && total > 0) {
                    const highest: number = page * size;
                    if (highest < total) {
                        urlObj.searchParams.set("page", (page + 1).toString());
                        links.next = urlObj.toString();
                    }
                }
            }
        }
        return links;
    }

}