import DBConnection from "./db/database-connection";
import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import Router from "./router";

// const express = require('express');
// const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 8080; // Server port
// const path = require("path");
console.log("Port", process.env.PORT, port);

app.use(express.static(path.join(__dirname, 'build')));
app.use(bodyParser.json());

const db: DBConnection = new DBConnection();
db.connect();

Router.setupRoutes(app);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
