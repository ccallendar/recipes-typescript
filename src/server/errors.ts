
export class ErrorObject {
    public message: string;
    public httpCode?: number;
    public request: any;
}

export interface IBaseErrorOptions {
    error?: Error;
    message?: string;
    params?: any;
}

export interface IBaseHttpErrorOptions extends IBaseErrorOptions {
    status?: number;
    statusText?: string;
}

export class BaseError extends Error {
    constructor(message: string, options?: IBaseErrorOptions) {
        super(message);
        Error.captureStackTrace(this, this.constructor);
        if (options) {
            Object.assign(this, options);
        }
    }
}

Object.assign(BaseError.prototype, ErrorObject.prototype);

export class BaseHttpError extends BaseError {
    public status: number;
    public statusText: string;
    public readonly message: string;
    get httpCode(): number {
        return this.status;
    }

    constructor(message: string, options?: IBaseHttpErrorOptions) {
        super(message, options || {});
        Error.captureStackTrace(this, this.constructor);
    }

    public toString(): string {
        return this.message;
    }
}

export class BadRequestError extends BaseHttpError {
    public status: number = 400;
    public statusText: string = "Bad Request";
}

export class UnauthorizedError extends BaseHttpError {
    public status: number = 401;
    public statusText: string = "Unauthorized";
}

export class ForbiddenRequestError extends BaseHttpError {
    public status: number = 403;
    public statusText: string = "Forbidden";
}

export class NotFoundError extends BaseHttpError {
    public status: number = 404;
    public statusText: string = "Not Found";
}

export class ConflictError extends BaseHttpError {
    public status: number = 409;
    public statusText: string = "Conflict";
}

export class InternalServerError extends BaseHttpError {
    public status: number = 500;
    public statusText: string = "Internal Server Error";
}

export class NotImplementedError extends BaseHttpError {
    public status: number = 501;
    public statusText: string = "Not Implemented";
}