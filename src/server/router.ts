import SwaggerUI from "swagger-ui-express";
import swaggerJSDoc from "swagger-jsdoc";
import {Application, Request, Response} from "express";
import path from "path";
import RecipesController from "./controllers/RecipesController";
import MealsController from "./controllers/MealsController";
import RatingsController from "./controllers/RatingsController";
// tslint:disable-next-line:no-var-requires
const pjson = require("../../package.json");

/**
 * @swagger
 * parameters:
 *   recipeId:
 *     name: "recipeId"
 *     description: Recipe Id
 *     in: path
 *     required: true
 *     type: number
 *   id:
 *     name: "id"
 *     description: Id
 *     in: path
 *     required: true
 *     type: number
 *   searchQuery:
 *     name: "key"
 *     description: Search Query
 *     in: query
 *     required: false
 *     type: string
 *   page:
 *     name: "page"
 *     description: Page number
 *     in: query
 *     required: false
 *     type: integer
 *     default: 1
 *   size:
 *     name: "size"
 *     description: Page size
 *     in: query
 *     required: false
 *     type: integer
 *     default: 100
 */

export default class Router {
    static setupRoutes(app: Application) {
        const recipes = new RecipesController();
        const meals = new MealsController();
        const ratings = new RatingsController();

        app.get('/', (req, res) => {
            res.sendFile(path.join(__dirname + "/build", 'index.html'));
        });
        app.get('/ping', (req, res) => {
            res.send("pong");
        });

        app.get('/api/categories', (req: Request, res: Response) => recipes.getCategories(req, res));

        app.get('/api/recipes', (req: Request, res: Response) => recipes.getAll(req, res));
        app.get('/api/recipes/search', (req: Request, res: Response) => recipes.search(req, res));
        app.get('/api/recipe/:recipeId', (req: Request, res: Response) => recipes.findById(req, res));
        app.post('/api/recipe', (req: Request, res: Response) => recipes.create(req, res));
        app.put('/api/recipe/:recipeId', (req: Request, res: Response) => recipes.update(req, res));

        app.get('/api/meals', (req: Request, res: Response) => meals.getAll(req, res));
        app.get('/api/meal/:id', (req: Request, res: Response) => meals.findById(req, res));
        app.post('/api/meal', (req: Request, res: Response) => meals.create(req, res));
        app.put('/api/meal/:id', (req: Request, res: Response) => meals.update(req, res));

        app.get('/api/ratings/:recipeId', (req: Request, res: Response) => ratings.findRatingsForRecipe(req, res));
        app.get('/api/rating/:id', (req: Request, res: Response) => ratings.findById(req, res));
        app.post('/api/rating', (req: Request, res: Response) => ratings.create(req, res));

        Router.setupSwagger(app);
    }

    static setupSwagger(app: Application) {
        const options = {
            apis: ["./src/server/**/*.ts"],
            swaggerDefinition: {
                info: {
                    description: pjson.description,
                    title: pjson.name,
                    version: pjson.version,
                    contact: {
                        name: pjson.author,
                    },
                    servers: ['http://localhost:8080']
                },
            },
        };

        const swaggerDocs = swaggerJSDoc(options);
        app.use("/docs", SwaggerUI.serve, SwaggerUI.setup(swaggerDocs));
    }

}