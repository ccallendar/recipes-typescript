import Database from "../src/server/db/database";

describe("Database test", () => {
  const db = new Database();
  test("it counts the recipes", async () => {
    const count = await db.getRecipesCount();
    expect(count).toBeGreaterThan(0);
  });
});